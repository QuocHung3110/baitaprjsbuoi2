import React, { Component } from 'react'
import "../Components-css/Body.css"
import Weared from './Body/Weared'
import None from './Body/None'
import Glasses from './Body/Glasses'
import {data} from './dataGlasses'

export default class Body extends Component {
  state = {
    glassesList:data,
    item:{},
  }
  handleItem=(object)=>{
    this.setState({
      item:object,
    })
  }
  render() {
    return (
      <div className="container" id='body'>
        <div className="Body-content row ">
        <Weared item={this.state.item}/>
        <None />
        <Glasses list={this.state.glassesList} handleOnCLick={this.handleItem}/>
        </div>
      </div>
    )
  }
}
