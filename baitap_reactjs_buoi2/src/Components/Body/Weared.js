import React, { Component } from 'react'
import '../../Components-css/Body/Weared.css'
export default class Weared extends Component {
  
  render() {
    let {url,name,desc}=this.props.item;
    return (
      <div className='col-6 ' id='Weared'>
          <div style={{height:'34%'}}><img src={url}/></div>
          <div className='text-left pl-1' style={{background:'#f2748661',width:'63.5%', margin:'26% 0 0 18.4%'}}>
            <h4 className='text-primary'>{name}</h4>
            <p className='text-white font-weight-bold'>{desc}</p>
          </div>
      </div>
    )       
  }
}