import React, { Component } from "react";
import "../../Components-css/Body/Glasses.css";

export default class Glasses extends Component {
  renderList = () => {
    let contentHTML = this.props.list.map((item, index) => {
      return (
        <div className="col-2">
          <button
            className="btn btn-white btn-outline-info"
            onClick={()=>this.props.handleOnCLick(item)}
          >
            <img src={item.url} width="100%" />
          </button>
        </div>
      );
    });
    return contentHTML;
  };
  render() {
    return (
      <div className="row border p-4 bg-white rounded" id="Glasses">
        {this.renderList()}
      </div>
    );
  }
}
